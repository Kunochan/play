Play
====

Play is a utility which chooses the next episode of a show to watch when accessing videos via command line. It records the number previously watched episode in a text file stored in the same folder as the videos. When run, play increments this number by one, and then sorts the episodes in the folder	alphabetically and plays the episode which corresponds with the number. If the count file does not exist, it will create one with the count set at 1 and play the first episode. If no episodes match the current episode, the count will reset to zero and the scrip will terminate after notifying the user.

If used properly, and the video files are in a sane format (IE with episode numbers), using this command consecutively will run through all the videos in order.

License
========
This script may only be used to play illegally obtained video files, and for other such activities undermining the concept of intellectual property.
PROPERTY IS THEFT!
FUCK THE POLICE!

Pseudo Code
===========
```
NEWCOUNT(num) = function
    NUMBER = num
    create a text file named "count", overwrite any existing file with that name.
    write NUMBER on file
PLAYER = mpv #change this line to whatever video player you want to invoke
FORMATS = [".mp4",".avi",".mkv",".flv",etc...]
Check to see if COUNT file exists
IF count file does not exist:
   NEWCOUNT(0)
   Continue
Open COUNT
NUMBER = first line of COUNT
NUMBER += 1
Get list of files in folder
Put all the video files (defined by FORMATS list) in ARRAY
Sort ARRAY alphanumerically #If the files are named sensibly, there will be zero padded episode numbers before the episode titles
NEXTEPISODE = the episode at the array index which corresponds with NUMBER
IF NUMBER is out of array:
   NEWCOUNT(0)
   Inform user that the series is complete
   Break
NEWCOUNT(NUMBER)
Run PLAYER NEXTEPISODE
```